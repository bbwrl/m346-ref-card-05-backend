package ch.bbw.backend;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class Message {
    private String text;
}
